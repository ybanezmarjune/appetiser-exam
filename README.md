# iTunes Exam App

This app is fetching data from itunes API

## Installation

- Download or clone project
```bash
pod install
```

## Architecture

- MVVM 

## What I Done?
Hi, what I do is only 2 View Controllers. The first controller is Homepage and the second is Details

#### - Homepage ViewController (Types of section)
- Feature cell 
   - Used API: ```https://rss.itunes.apple.com/api/v1/ph/movies/top-movies/action-and-adventure/25/explicit.json```
- List Cell
   - Used API: ```https://itunes.apple.com/search```

#### - Detail ViewController (Types of section)
- Header cell
   - Poster, genre, price, and title.
- Details cell
   - Long details or description of the movie
- Trailer cell
   - Basically play video from fetched API response


## License
[MIT](https://choosealicense.com/licenses/mit/)