//
//  Endpoint.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Alamofire
import Foundation

protocol Endpoint {
    var method: HTTPMethod { get }
    var path: String { get }
    var query: [String: Any] { get }
    var encoding: ParameterEncoding { get }
}

extension Endpoint {
    // swiftlint:disable unused_setter_value
    var encoding: ParameterEncoding { get { return URLEncoding.default } set {} }
    // swiftlint:enable unused_setter_value
}
