//
//  AFRequestInterceptor.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Alamofire
import Foundation
import SVProgressHUD

final class AFRequestInterceptor: Alamofire.RequestInterceptor {
    private var accessToken: String
    typealias AdapterResult = Swift.Result<URLRequest, Error>

    init(token: String) {
        self.accessToken = token
    }

    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        guard let response = request.task?.response as? HTTPURLResponse, let errorCode = APIError(rawValue: response.statusCode) else {
            return completion(.doNotRetryWithError(error))
        }

        switch errorCode {
        case .unauthorized:
            debugPrint("******** UNAUTHORIZED ********")
            return completion(.doNotRetry)
        case .timeOut:
            debugPrint("******** REQUEST TIME OUT ********")
            debugPrint("Retry Count = \(request.retryCount)")
            debugPrint("requested URL = \(String(describing: response.url))")
            if request.retryCount == 3 {
                return completion(.doNotRetry) }
            return completion(.retryWithDelay(1.0)) // retry after 1 second
        case .invalidParam:
            debugPrint("************ ============ ************")
            debugPrint("******* WRONG PARAMETER SEND TO API *******")
            completion(.doNotRetry)
        case .notFound:
            debugPrint("************ ============ ************")
            debugPrint("******* NOT FOUND IN SERVER *******")
            completion(.doNotRetry)
        case .dependencyFail:
            debugPrint("************ ============ ************")
            debugPrint("******* BACKEND FAILD FOR DEPENDENCY *******")
            completion(.doNotRetry)
        case .serverProblem:
            debugPrint("************ ============ ************")
            debugPrint("******* BACKEND INTERNAL SERVER PROBLEM *******")
            completion(.retry)
        case .preconditioned:
            debugPrint("************ ============ ************")
            debugPrint("******* PRE CONDITION FAILED *******")
            completion(.doNotRetry)
        }
    }
}
