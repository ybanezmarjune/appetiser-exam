//
//  SearchEndPoint.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Foundation
import Alamofire

enum SearchEndPoint: Endpoint {
    case getAppstoreList
    case getTopMovies
    case searchMovie(id: String)
    
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .getAppstoreList:
            return "https://itunes.apple.com/search"
        case .getTopMovies:
            /// Get more json sample here: https://rss.itunes.apple.com/en-us
            return "https://rss.itunes.apple.com/api/v1/ph/movies/top-movies/action-and-adventure/25/explicit.json"
        case .searchMovie:
            return "https://itunes.apple.com/lookup"
        }
    }
    
    var query: [String : Any] {
        switch self {
        case .getAppstoreList:
            return ["attribute": "movieTerm", "limit": "50", "entity": "movie"]
        case .getTopMovies:
            return [:]
        case .searchMovie(let id):
            return ["id": id]
        }
    }
}
