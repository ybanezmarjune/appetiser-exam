//
//  UIStoryboard+Ext.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    /// FileName of storboards you created
    enum Storyboard: String {
        case main = "Main"
    }
}

extension UIStoryboard.Storyboard {
    var filename: String {
        return rawValue
    }
    
    var instance: UIStoryboard {
        return UIStoryboard(name: filename, bundle: Bundle.main)
    }
    
    func instantiateViewController<T: UIViewController>() -> T {
        guard let viewController = instance.instantiateViewController(withIdentifier: String(describing: T.self)) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.self) ")
        }
        
        return viewController
    }
}

extension UIViewController {
    static func load(from storyboard: UIStoryboard.Storyboard) -> Self {
        return storyboard.instantiateViewController()
    }
}
