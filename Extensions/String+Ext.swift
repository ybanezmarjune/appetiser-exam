//
//  String+Ext.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Foundation

extension String {
    func toUrl() -> URL? {
        return URL(string: self)
    }
}
