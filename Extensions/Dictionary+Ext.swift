//
//  Dictionary+Ext.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Foundation

extension Sequence {
    func groupToArray<U: Hashable>(by key: (Iterator.Element) -> U) -> [[Iterator.Element]] {
        var groups: [U: [Iterator.Element]] = [:]
        var groupsOrder: [U] = []
        forEach { element in
            let key = key(element)
            if case nil = groups[key]?.append(element) {
                groups[key] = [element]
                groupsOrder.append(key)
            }
        }
        return groupsOrder.map { groups[$0]! }
    }
}
