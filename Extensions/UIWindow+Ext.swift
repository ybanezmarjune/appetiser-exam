//
//  UIWindow+Ext.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import UIKit

extension UIWindow {
    // MARK: - Get Visible Controller
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController  = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(rootViewController)
            
        }
        return nil
    }
    
    class func getVisibleViewControllerFrom(_ vc:UIViewController) -> UIViewController {
        var nextOnStackViewController: UIViewController? = nil
        if let presented = vc.presentedViewController {
            nextOnStackViewController = presented
        } else if let navigationController = vc as? UINavigationController,
            let visible = navigationController.visibleViewController {
            nextOnStackViewController = visible
        } else if let tabBarController = vc as? UITabBarController,
            let visible = (tabBarController.selectedViewController ??
                tabBarController.presentedViewController) {
            nextOnStackViewController = visible
        }
        
        if let nextOnStackViewController = nextOnStackViewController {
            return getVisibleViewControllerFrom(nextOnStackViewController)
        } else {
            return vc
        }
    }
}
