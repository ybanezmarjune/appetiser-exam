//
//  Cell+Ext.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Foundation
import UIKit

extension UITableViewCell {
    public static var id: String {
        return "\(self)"
    }
    public static var nibFile: UINib {
        return UINib(nibName: id, bundle: nil)
    }
}


extension UICollectionViewCell {
    public static var id: String {
        return "\(self)"
    }
    public static var nibFile: UINib {
        return UINib(nibName: id, bundle: nil)
    }
}
