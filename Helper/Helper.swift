//
//  Helper.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import UIKit

struct Helper {
    
    // MARK: - Alert Helpers
    static func showAlert(msg: String) {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        let mwindow = UIApplication.shared.connectedScenes
            .filter({ $0.activationState == .foregroundActive })
            .map({ $0 as? UIWindowScene })
            .compactMap({ $0 })
            .first?.windows
            .filter({ $0.isKeyWindow }).first
        guard let parentVC = mwindow?.visibleViewController() else { return }
        parentVC.present(alert, animated: true, completion: nil)
    }

}
