//
//  HomeCellViewResponder.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Foundation

protocol HomeCellViewResponder {
    func didSelect(indexPath: IndexPath)
}
