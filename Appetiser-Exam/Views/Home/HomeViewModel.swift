//
//  HomeViewModel.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Foundation
import SwiftyJSON
import SVProgressHUD

class HomeViewModel {
    // MARK: - Dynamic properties
    let didSelectItem = Dynamic<IndexPath?>(value: nil)
    let willReloadDisplay = Dynamic<Bool>(value: false)
    
    enum SectionType: CaseIterable {
        case feature
        case list
    }
    
    private let section = SectionType.allCases
    
    // MARK: - List Data
    private var listData: [SearchDataModelResult]!
    
    // MARK: - Feature Data
    private var featureData: [FeatureDataModelResult]!
    
    init() {
        SVProgressHUD.show()
        fetchTopMovies()
        fetchDefaultAPI()
    }
}

// MARK: - TableView Data
extension HomeViewModel {
    func getSection(in indexPath: IndexPath) -> SectionType {
        return self.section[indexPath.section]
    }
    
    func numberSection() -> Int {
        return section.count
    }
    
    func numberRowIn(section: Int) -> Int {
        switch self.section[section] {
        case .feature:
            return featureData == nil ? 0 : 1
        case .list:
            return listData == nil ? 0 : listData.count
        }
    }
    
    func featureItem(in indexPath: IndexPath) -> [FeatureDataModelResult] {
        return featureData
    }
    
    func listItem(in indexPath: IndexPath) -> SearchDataModelResult {
        return listData[indexPath.row]
    }
    
    func tableViewHeight(in indexPath: IndexPath) -> CGFloat {
        switch self.section[indexPath.section] {
        case .feature:
            return 400.0
        default:
            return UITableView.automaticDimension
        }
    }

    func tableViewDidSelect(in indexPath: IndexPath) -> String {
        switch self.section[indexPath.section] {
        case .feature:
            /// return url
            return featureData[indexPath.row].url ?? ""
        case .list:
            return "\(listData[indexPath.row].trackID ?? 0)"
        }
    }
}

// MARK: - API Call
extension HomeViewModel {
    func fetchDefaultAPI() {
        APIClient.shared.objectAPICall(apiEndPoint: SearchEndPoint.getAppstoreList,
                                       modelType: SearchDataModel.self) { response in
            switch response {
            case .success(let data):
                self.listData = data.results
                self.willReloadDisplay.value = true
            case .failure(let error):
                Helper.showAlert(msg: error.2.localizedDescription)
            }
        }
    }
    
    func fetchTopMovies() {
        APIClient.shared.objectAPICall(apiEndPoint: SearchEndPoint.getTopMovies,
                                       modelType: FeatureDataModel.self) { response in
            
            SVProgressHUD.dismiss()
            switch response {
            case .success(let data):
                self.featureData = data.feed?.results
                self.willReloadDisplay.value = true
            case .failure(let error):
                Helper.showAlert(msg: error.2.localizedDescription)
            }
        }
    }
}
