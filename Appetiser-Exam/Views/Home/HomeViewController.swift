//
//  HomeViewController.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    
    let viewModel = HomeViewModel()
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareTableView()
        bindViewModel()
    }
    
    // MARK: - Prepare TableView
    private func prepareTableView() {
        tableView.register(HomeTableViewCell.nibFile, forCellReuseIdentifier: HomeTableViewCell.id)
        tableView.register(ListTableViewCell.nibFile, forCellReuseIdentifier: ListTableViewCell.id)
        tableView.dataSource = self
        tableView.delegate = self
    }
}

// MARK: - Bind ViewModel
extension HomeViewController {
    func bindViewModel() {
        viewModel.willReloadDisplay.bind { [weak self] reload in
            guard reload else { return }
            self?.tableView.reloadData()
        }
    }
}

// MARK: - TableView Datasource
extension HomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberSection()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberRowIn(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel.getSection(in: indexPath) {
        case .feature:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.id, for: indexPath) as? HomeTableViewCell else { return UITableViewCell() }
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            cell.cellDisplay(item: viewModel.featureItem(in: indexPath), responder: self)
            return cell
        case .list:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ListTableViewCell.id, for: indexPath) as? ListTableViewCell else { return UITableViewCell() }
            cell.backgroundColor = .clear
            cell.accessoryType = .disclosureIndicator
            cell.selectionStyle = .none
            cell.cellDisplay(item: viewModel.listItem(in: indexPath))
            return cell
        }
    }
}

// MARK: - TableView Delegate
extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.tableViewHeight(in: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = viewModel.tableViewDidSelect(in: indexPath)
        presentItemDetail(id: id)
    }
}

// MARK: - Responder Feature
extension HomeViewController: HomeCellViewResponder {
    func didSelect(indexPath: IndexPath) {
        guard let url = viewModel.tableViewDidSelect(in: indexPath).toUrl() else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}

// MAKR: - Present
extension HomeViewController {
    func presentItemDetail(id: String) {
        let controller = ItemDetailViewController.load(from: .main)
        controller.viewModel = ItemDetailViewModel(id: id)
        let navigation = UINavigationController(rootViewController: controller)
        self.present(navigation, animated: true, completion: nil)
    }
}
