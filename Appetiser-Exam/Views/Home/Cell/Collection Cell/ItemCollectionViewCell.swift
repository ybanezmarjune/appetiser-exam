//
//  ItemCollectionViewCell.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import UIKit
import SDWebImage

class ItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageViewArtWork: UIImageView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelGenre: UILabel!
    @IBOutlet var buttonVisit: UIButton! {
        didSet {
            buttonVisit.setCornerRadius(12)
            buttonVisit.setBorderWidth(1.0)
            buttonVisit.setBorderColor(.systemBlue)
            buttonVisit.addTarget(self, action: #selector(didTapVisit), for: .touchUpInside)
        }
    }
    var item: FeatureDataModelResult?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Reused
    override func prepareForReuse() {
        super.prepareForReuse()
        
        item = nil
        labelTitle.text = ""
        labelGenre.text = ""
        imageViewArtWork.image = nil
    }
}

// MARK: - Cell Display
extension ItemCollectionViewCell {
    func cellDisplay(item: FeatureDataModelResult) {
        self.item = item
        labelGenre.text = item.genres?.compactMap { $0.name }.joined(separator: ", ")
        labelTitle.text = item.name
        imageViewArtWork.sd_setImage(with: item.artworkUrl100?.toUrl(),
                                     placeholderImage: UIImage(named: "ic_home_bg"),
                                     completed: nil)
    }
    
    @objc func didTapVisit() {
        guard let url = item?.url?.toUrl() else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
