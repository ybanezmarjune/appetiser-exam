//
//  ListTableViewCell.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    @IBOutlet var imageViewPoster: UIImageView!
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelGenre: UILabel!
    @IBOutlet var buttonBuy: UIButton! {
        didSet {
            buttonBuy.setCornerRadius(12)
            buttonBuy.setBorderWidth(1.0)
            buttonBuy.setBorderColor(.systemBlue)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

// MARK: - Cell Display
extension ListTableViewCell {
    func cellDisplay(item: SearchDataModelResult) {
        labelName.text = item.trackName
        labelGenre.text = item.primaryGenreName
        buttonBuy.setTitle(getPrice(item: item), for: .normal)
        imageViewPoster.sd_setImage(with: item.artworkUrl100?.toUrl(),
                                     placeholderImage: UIImage(named: "ic_home_bg"),
                                     completed: nil)
    }
    
    func getPrice(item: SearchDataModelResult) -> String {
        if let price = item.trackPrice {
            return "₱ \(price) BUY"
        }
        if let price = item.trackHDRentalPrice {
            return "₱ \(price) RENT"
        }
        if let price = item.trackHDPrice {
            return "₱ \(price) BUY"
        }
        buttonBuy.isHidden = true
        return ""
    }
}
