//
//  HomeTableViewCell.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    @IBOutlet var collectionView: UICollectionView!
    private var delegate: HomeCellViewResponder?
    var dataModel = [FeatureDataModelResult]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareTableView()
        startTimer()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialized
    private func prepareTableView() {
        collectionView.register(ItemCollectionViewCell.nibFile,
                                forCellWithReuseIdentifier: ItemCollectionViewCell.id)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

// MARK: - Cell Display
extension HomeTableViewCell {
    func cellDisplay(item: [FeatureDataModelResult], responder: HomeCellViewResponder) {
        delegate = responder
        dataModel = item
        collectionView.reloadData()
    }
}

// MARK: - UICollection Datasource
extension HomeTableViewCell: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCollectionViewCell.id, for: indexPath) as? ItemCollectionViewCell else { return UICollectionViewCell() }
        cell.cellDisplay(item: dataModel[indexPath.row])
        return cell
    }
}

// MARK: - AutoScroll
extension HomeTableViewCell {
    @objc func scrollToNextCell(){
        let cellSize = CGSize(width: frame.width, height: frame.height);
        let contentOffset = collectionView.contentOffset;
        collectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func startTimer() {
        Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
    }
}

// MARK: - Collection Layout
extension HomeTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2.3, height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelect(indexPath: indexPath)
    }
}
