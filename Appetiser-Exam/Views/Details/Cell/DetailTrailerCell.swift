//
//  DetailTrailerCell.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import UIKit
import BMPlayer

class DetailTrailerCell: UITableViewCell {
    @IBOutlet var playerView: BMPlayer!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension DetailTrailerCell {
    func cellDisplay(item: SearchDataModelResult) {
        guard let url = item.previewURL?.toUrl() else { return }
        playVideo(url: url)
    }
    
    func playVideo(url: URL){
        let asset = BMPlayerResource(url: url)
        playerView.setVideo(resource: asset)
    }
}
