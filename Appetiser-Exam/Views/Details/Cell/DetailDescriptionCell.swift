//
//  DetailDescriptionCell.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import UIKit

class DetailDescriptionCell: UITableViewCell {
    @IBOutlet var labelDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

// MARK: - Display Cell
extension DetailDescriptionCell {
    func cellDisplay(item: SearchDataModelResult) {
        labelDescription.text = item.longDescription
    }
}
