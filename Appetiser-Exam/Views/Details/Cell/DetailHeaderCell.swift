//
//  DetailHeaderCell.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import UIKit

class DetailHeaderCell: UITableViewCell {
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var imageViewPoster: UIImageView!
    @IBOutlet var labelGender: UILabel!
    @IBOutlet var labelCountry: UILabel!
    @IBOutlet var labelReleaseYear: UILabel!
    @IBOutlet var buttonBuy: UIButton! {
        didSet {
            buttonBuy.setCornerRadius(12)
            buttonBuy.setBorderWidth(1.0)
            buttonBuy.setBorderColor(.systemBlue)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

// MARK: - Display Cell
extension DetailHeaderCell {
    func cellDisplay(item: SearchDataModelResult) {
        labelTitle.text = item.trackName
        labelGender.text = item.primaryGenreName
        labelCountry.text = item.country
        labelReleaseYear.text = item.releaseDate
        buttonBuy.setTitle(getPrice(item: item), for: .normal)
        imageViewPoster.sd_setImage(with: item.artworkUrl100?.toUrl(),
                                     placeholderImage: UIImage(named: "ic_home_bg"),
                                     completed: nil)
    }
    
    func getPrice(item: SearchDataModelResult) -> String {
        if let price = item.trackPrice {
            return "₱ \(price) BUY"
        }
        if let price = item.trackHDRentalPrice {
            return "₱ \(price) RENT"
        }
        if let price = item.trackHDPrice {
            return "₱ \(price) BUY"
        }
        buttonBuy.isHidden = true
        return ""
    }
}
