//
//  ItemDetailViewModel.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Foundation
import UIKit
import SVProgressHUD

class ItemDetailViewModel {
    // MARK: - Properties
    private var detailData: SearchDataModelResult!
    private var section = SectionType.allCases
    
    /// Dynamic
    let willReloadDisplay = Dynamic<Bool>(value: false)
    
    enum SectionType: CaseIterable {
        case header
        case details
        case trailer
    }
    
    var itemId: String
    init(id: String) {
        itemId = id
    }
    
    func getTitle() -> String {
        return detailData == nil ? "" : detailData.trackName ?? ""
    }
}

// MARK: - TableView
extension ItemDetailViewModel {
    func numberOfSection() -> Int {
        return section.count
    }
    
    func numberOfRow(in section: Int) -> Int {
        return detailData == nil ? 0 : 1
    }
    
    func cellItem(in indexPath: IndexPath) -> SearchDataModelResult {
        return detailData
    }
    
    func getSection(in indexPath: IndexPath) -> SectionType {
        return self.section[indexPath.section]
    }
    
    func tableViewHeight(in indexPath: IndexPath) -> CGFloat {
        return self.section[indexPath.section] == .details ? 400 : UITableView.automaticDimension
    }
}

// MARK: - API Call
extension ItemDetailViewModel {
    func fetchSearchItem() {
        SVProgressHUD.show()
        APIClient.shared.objectAPICall(apiEndPoint: SearchEndPoint.searchMovie(id: self.itemId),
                                       modelType: SearchDataModel.self) { response in
            switch response {
            case .success(let data):
                guard let result = data.results else { return }
                self.detailData = result.first
                self.willReloadDisplay.value = true
            case .failure(let error):
                Helper.showAlert(msg: error.2.localizedDescription)
            }
            SVProgressHUD.dismiss()
        }
    }
}
