//
//  ItemDetailViewController.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import UIKit

class ItemDetailViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    var viewModel: ItemDetailViewModel! {
        didSet {
            bindViewModel()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigation()
        prepareTableView()
        viewModel.fetchSearchItem()
    }

    // MARK: - Prepare
    private func prepareNavigation() {
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_close"), style: .done, target: self, action: #selector(didTapBack))
        navigationItem.setLeftBarButton(backButton, animated: true)
    }
    
    private func prepareTableView() {
        tableView.register(DetailHeaderCell.nibFile, forCellReuseIdentifier: DetailHeaderCell.id)
        tableView.register(DetailDescriptionCell.nibFile, forCellReuseIdentifier: DetailDescriptionCell.id)
        tableView.register(DetailTrailerCell.nibFile, forCellReuseIdentifier: DetailTrailerCell.id)
        tableView.dataSource = self
        tableView.delegate = self
    }
}

// MARK: - Bind ViewModel
extension ItemDetailViewController {
    func bindViewModel() {
        viewModel.willReloadDisplay.bind { [weak self] reload in
            guard reload else { return }
            self?.tableView.reloadData()
            self?.navigationItem.title = self?.viewModel.getTitle()
        }
    }
}

// MARK: - Action
extension ItemDetailViewController {
    @objc func didTapBack() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableView Datasource
extension ItemDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSection()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRow(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel.getSection(in: indexPath) {
        case .header:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailHeaderCell.id, for: indexPath) as? DetailHeaderCell else { return UITableViewCell() }
            cell.cellDisplay(item: viewModel.cellItem(in: indexPath))
            return cell
        case .details:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailDescriptionCell.id, for: indexPath) as? DetailDescriptionCell else { return UITableViewCell() }
            cell.cellDisplay(item: viewModel.cellItem(in: indexPath))
            return cell
        case .trailer:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailTrailerCell.id, for: indexPath) as? DetailTrailerCell else { return UITableViewCell() }
            cell.cellDisplay(item: viewModel.cellItem(in: indexPath))
            return cell
        }
    }
}

extension ItemDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
