//
//  SearchDataModel.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let searchDataModel = try? newJSONDecoder().decode(SearchDataModel.self, from: jsonData)

//
// Hashable or Equatable:
// The compiler will not be able to synthesize the implementation of Hashable or Equatable
// for types that require the use of JSONAny, nor will the implementation of Hashable be
// synthesized for types that have collections (such as arrays or dictionaries).

import Foundation

// MARK: - SearchDataModel
struct SearchDataModel: Codable, Equatable {
    var resultCount: Int?
    var results: [SearchDataModelResult]?
}


// MARK: - SearchDataModelResult
struct SearchDataModelResult: Codable, Equatable {
    var wrapperType, kind: String?
    var artistID, collectionID, trackID: Int?
    var artistName, collectionName, trackName, collectionCensoredName: String?
    var trackCensoredName: String?
    var artistViewURL, collectionViewURL, trackViewURL: String?
    var previewURL: String?
    var artworkUrl30, artworkUrl60, artworkUrl100: String?
    var collectionPrice, trackPrice: Double?
    var releaseDate, collectionExplicitness, trackExplicitness: String?
    var discCount, discNumber, trackCount, trackNumber: Int?
    var trackTimeMillis: Int?
    var country, currency, primaryGenreName: String?
    var isStreamable: Bool?
    var contentAdvisoryRating, copyright, resultDescription: String?
    var amgArtistID, collectionArtistID: Int?
    var collectionArtistViewURL: String?
    var trackRentalPrice, collectionHDPrice, trackHDPrice, trackHDRentalPrice: Double?
    var shortDescription, longDescription: String?
    var hasITunesExtras: Bool?

    enum CodingKeys: String, CodingKey {
        case wrapperType, kind
        case artistID = "artistId"
        case collectionID = "collectionId"
        case trackID = "trackId"
        case artistName, collectionName, trackName, collectionCensoredName, trackCensoredName
        case artistViewURL = "artistViewUrl"
        case collectionViewURL = "collectionViewUrl"
        case trackViewURL = "trackViewUrl"
        case previewURL = "previewUrl"
        case artworkUrl30, artworkUrl60, artworkUrl100, collectionPrice, trackPrice, releaseDate, collectionExplicitness, trackExplicitness, discCount, discNumber, trackCount, trackNumber, trackTimeMillis, country, currency, primaryGenreName, isStreamable, contentAdvisoryRating, copyright
        case resultDescription = "description"
        case amgArtistID = "amgArtistId"
        case collectionArtistID = "collectionArtistId"
        case collectionArtistViewURL = "collectionArtistViewUrl"
        case trackRentalPrice
        case collectionHDPrice = "collectionHdPrice"
        case trackHDPrice = "trackHdPrice"
        case trackHDRentalPrice = "trackHdRentalPrice"
        case shortDescription, longDescription, hasITunesExtras
    }
}
