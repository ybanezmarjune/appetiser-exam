//
//  FeatureDataModel.swift
//  Appetiser-Exam
//
//  Created by Marjune Ybanez on 6/14/21.
//

import Foundation

// MARK: - FeatureDataModel
struct FeatureDataModel: Codable, Equatable {
    var feed: FeatureDataModelFeed?
}

//
// Hashable or Equatable:
// The compiler will not be able to synthesize the implementation of Hashable or Equatable
// for types that require the use of JSONAny, nor will the implementation of Hashable be
// synthesized for types that have collections (such as arrays or dictionaries).

// MARK: - FeatureDataModelFeed
struct FeatureDataModelFeed: Codable, Equatable {
    var title: String?
    var id: String?
    var author: FeatureDataModelAuthor?
    var links: [FeatureDataModelLink]?
    var copyright, country: String?
    var icon: String?
    var updated: String?
    var results: [FeatureDataModelResult]?
}

//
// Hashable or Equatable:
// The compiler will not be able to synthesize the implementation of Hashable or Equatable
// for types that require the use of JSONAny, nor will the implementation of Hashable be
// synthesized for types that have collections (such as arrays or dictionaries).

// MARK: - FeatureDataModelAuthor
struct FeatureDataModelAuthor: Codable, Equatable {
    var name: String?
    var uri: String?
}

//
// Hashable or Equatable:
// The compiler will not be able to synthesize the implementation of Hashable or Equatable
// for types that require the use of JSONAny, nor will the implementation of Hashable be
// synthesized for types that have collections (such as arrays or dictionaries).

// MARK: - FeatureDataModelLink
struct FeatureDataModelLink: Codable, Equatable {
    var linkSelf: String?
    var alternate: String?

    enum CodingKeys: String, CodingKey {
        case linkSelf = "self"
        case alternate
    }
}

//
// Hashable or Equatable:
// The compiler will not be able to synthesize the implementation of Hashable or Equatable
// for types that require the use of JSONAny, nor will the implementation of Hashable be
// synthesized for types that have collections (such as arrays or dictionaries).

// MARK: - FeatureDataModelResult
struct FeatureDataModelResult: Codable, Equatable {
    var artistName, id, releaseDate, name: String?
    var kind, copyright: String?
    var artworkUrl100: String?
    var genres: [FeatureDataModelGenre]?
    var url: String?
    var artistID: String?
    var artistURL: String?

    enum CodingKeys: String, CodingKey {
        case artistName, id, releaseDate, name, kind, copyright, artworkUrl100, genres, url
        case artistID = "artistId"
        case artistURL = "artistUrl"
    }
}

//
// Hashable or Equatable:
// The compiler will not be able to synthesize the implementation of Hashable or Equatable
// for types that require the use of JSONAny, nor will the implementation of Hashable be
// synthesized for types that have collections (such as arrays or dictionaries).

// MARK: - FeatureDataModelGenre
struct FeatureDataModelGenre: Codable, Equatable {
    var genreID, name: String?
    var url: String?

    enum CodingKeys: String, CodingKey {
        case genreID = "genreId"
        case name, url
    }
}
